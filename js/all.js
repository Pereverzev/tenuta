$(document).ready(function($){
	$('.j-nav__link').on('click', function(e){
		e.preventDefault();
		showSection($(this).attr('href'), true);
	});
	$('.j-logo').on('click', function(e){
		e.preventDefault();
		showSection($(this).attr('href'), true);
	});
	showSection(window.location.hash, false);
	checkSection();
});

$(window).on('scroll', function(){
	checkSection();
	stickyHeader();
});
function stickyHeader(){
	var header = $('.b-header'),
		windowHeight = $(window).height(),
		wScroll = $(window).scrollTop();

	if(wScroll >= windowHeight){
		$(header).addClass('m-header_active');	
	}else{
		$(header).removeClass('m-header_active');
	}
}
function checkSection(){
	$('.b-section').each(function(){
		var self = $(this),
			topEdge = self.offset().top - 200,
			bottomEdge = topEdge + self.height(),
			wScroll = $(window).scrollTop();

		if(topEdge < wScroll && bottomEdge > wScroll){
			var currentId = self.data('section'),
				reqLink = $('.j-nav__link').filter('[href="#'+ currentId +'"]');

			reqLink.addClass('m-nav__link_active').parent().siblings('li').find('a').removeClass('m-nav__link_active');
			console.log(reqLink);
			window.location.hash = currentId;
		}
	});
}
function showSection(section, isAnimate){
	var direction = section.replace(/#/, ''),
		reqSection = $('.b-section').filter('[data-section="'+ direction +'"]'),
		reqSectionPos = reqSection.offset().top;

	if(isAnimate){
		$('body, html').animate({scrollTop: reqSectionPos}, 500);
	}else{
		$('body, html').scrollTop(reqSectionPos);
	}
}