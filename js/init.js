$(document).ready(function($) {

	/*section 1 slider*/
	var $slides = $('.j-slides');

	Hammer($slides[0]).on("swipeleft", function(e) {
		$slides.data('superslides').animate('next');
	});

	Hammer($slides[0]).on("swiperight", function(e) {
		$slides.data('superslides').animate('prev');
	});

	$slides.superslides({
		pagination: true,
		animation: 'fade',
		elements: {
			pagination: '.b-slider__nav',
			container: '.j-slides__container'
		},
		inherit_width_from: 'body',
		inherit_height_from: 'body'
	});

	var slider = $('.j-wines');
	var options = {
		pagerCustom: '.j-wines__pager',
		mode: 'fade',
		controls: false,
		onSliderLoad: function(){
			$('.j-wines__prev').on('click', function(){
				slider.goToPrevSlide();
			});
			$('.j-wines__next').on('click', function(){
				slider.goToNextSlide();
			});
		}
	};
	slider.bxSlider(options);

	/*section 3 slider*/
	$('.j-winery').bxSlider({
		pager: false
	});
});